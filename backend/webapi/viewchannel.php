<?php
include '../koneksi.php';
// $total_port = $_GET['p'];
  $query = "SELECT m_mesin.displayed_port,m_mesin.bay_code,m_mesin.bay_name,mesin.*,m_mesin.nama_gi,m_mesin.nama,network.*,c.id_modbus,c.type as power_type,m_mesin.lokasi,view_setting.total_port
  FROM mesin
  LEFT JOIN m_mesin
  ON mesin.kode_mesin = m_mesin.kode_mesin
  LEFT JOIN network 
  ON network.kode_mesin = m_mesin.kode_mesin
  LEFT JOIN daspower_config c
  on c.kode_mesin=m_mesin.kode_mesin
  cross join view_setting
  where 	view_setting.username = 'admin'
  ORDER BY mesin.port ASC";

$result = mysqli_query($db_handle,$query);
$array_data = array();
while($baris = mysqli_fetch_assoc($result))
{
  $array_data[]=$baris;
}
echo json_encode($array_data);
?>
