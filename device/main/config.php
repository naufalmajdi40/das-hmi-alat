 <div>
     <br>
 </div>
 <div class="page-content page-container" id="page-content">
     <div class="padding">
         <div class="container d-flex justify-content-center">
             <div class="col-sm-12">
                 <div class="mb-6">
                     <ul class="nav nav-pills" id="myTab" role="tablist">
                         <li class="nav-item"><a class="nav-link active" id="devicesetting-tab" data-toggle="tab" href="#deviceSetting" role="tab" aria-controls="home" aria-selected="true">Device Setting</a></li>
                         <li class="nav-item"><a class="nav-link" id="lansetting-tab" data-toggle="tab" href="#lanSetting" role="tab" aria-controls="profile" aria-selected="false">LAN Setting</a></li>
                         <li class="nav-item"><a class="nav-link" id="wifisetting-tab" data-toggle="tab" href="#wifiSetting" role="tab" aria-controls="profile" aria-selected="false">Wifi Setting</a></li>
                         <li class="nav-item"><a class="nav-link" id="system-tab" data-toggle="tab" href="#system" role="tab" aria-controls="profile" aria-selected="false">System</a></li>
                     </ul>
                 </div>
                 <div class="tab-content mb-6">
                     <div class="tab-pane fade show active" id="deviceSetting" role="tabpanel" aria-labelledby="devicesetting-tab">
                         <div class="my-3 text-white " style="background-color:#000000;border-radius: 5px;">
                             <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                 <h6>Device Setting</h6>
                             </div>
                             <div class="row" style="padding:0px 10px; margin-top : 10px;">
                                 <div class="col">
                                     <label class="jQKeyboard" for="code">Device Code</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="text" name="" id="code" disabled>
                                     <div id="ip"></div>
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="location">Location</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="location">
                                 </div>
                             </div>
                             <div style="margin-top: 20px;"></div>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="name">Device Name</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="name">
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="bot_name">Bot Name</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="bot_name" disabled>
                                 </div>

                             </div>
                             <div style="margin-top: 20px;"></div>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="code">Displayed Port</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="number" name="port" id="port">
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="code">Font Size</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="number" name="fontSize" id="fontSize">
                                 </div>
                             </div>
                             <div style="margin-top: 20px;"></div>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="code">Zoom Box Width</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="number" name="zWidth" id="zWidth">
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="code">Zoom Box Height</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="number" name="zHeight" id="zHeight">
                                 </div>
                             </div>
                             <div style="margin-top: 20px;"></div>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="mr-sm-2" for="inlineFormCustomSelect">Backlight Saver</label>
                                     <select class="custom-select mr-sm-2" name="backlight" id="backlight">
                                         <option value="10">10 Seconds</option>
                                         <option value="30">30 Seconds</option>
                                         <option value="60">60 Seconds</option>
                                         <option value="600">5 Minutes</option>
                                         <option value="600">10 Minutes</option>
                                         <option value="900">15 Minutes</option>
                                         <option value="1200">20 Minutes</option>
                                         <option value="1800">30 Minutes</option>
                                         <option value="3600">60 Minutes</option>
                                     </select>
                                     <!-- <label class="jQKeyboard" for="code">Backlight Saver</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" maxlength="6" size="6" type="backlight" name="backlight" id="backlight"> -->
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="code">PIN</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" maxlength="6" size="6" type="password" name="pin" id="pin">
                                 </div>
                             </div>
                             <br>

                             <button class="btn btn-success flex-fill font-weight-bold" id="btnsave" style="margin-left:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                 Save Device Configuration
                             </button>
                         </div>
                     </div>
                     <div class="tab-pane fade" id="lanSetting" role="tabpanel" aria-labelledby="lansetting-tab">
                         <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                             <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                 <h6>LAN Setting</h6>
                             </div>
                             <div class="form-check" style="margin:10px;">
                                 <input class="form-check-input" type="checkbox" id="dhcp" name="dhcp" value="1">
                                 <label class="jQKeyboard" for="ipserver">Enable DHCP</label>
                             </div>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="ipserver">IP SERVER</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="text" name="" id="ipserver">
                                     <div id="ip"></div>
                                 </div>
                             </div>
                             <hr>
                             
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="ipaddress">IP ADDRESS</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="ipaddress">
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="netmask">NETMASK</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="netmask">
                                 </div>

                             </div>
                             <hr>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="gateway">GATEWAY</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="gateway">
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="dns">DNS</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="dns">
                                 </div>
                             </div>

                             <hr>
                             <div class="form-check" style="margin:10px;">
                                 <input class="form-check-input" type="checkbox" id="enableIPLAN" name="enableIPLAN" value="1">
                                 <label class="jQKeyboard" for="enableIPLAN">Show IP Address Ethernet</label>
                             </div>
                             <hr>
                             <button class="btn btn-success flex-fill font-weight-bold" id="btnsavelan" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                 Save LAN Configuration
                             </button>
                             <!-- <div class="d-flex justify-content-between flex-fill">
                                 <button class="btn btn-danger  flex-fill font-weight-bold" id="btncancel" style="height:40px;"> <i class="fas fa-chevron-circle-left"></i> &nbsp
                                     CANCEL
                                 </button>
                                 <button class="btn btn-success flex-fill font-weight-bold" id="btnsave" style="height:40px;"><i class="fas fa-check-circle"></i>
                                     &nbsp
                                     SAVE
                                 </button>
                             </div> -->
                         </div>
                     </div>
                     <div class="tab-pane fade" id="wifiSetting" role="tabpanel" aria-labelledby="wifisetting-tab">
                         <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                             <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                 <h6>Wifi Setting</h6>
                             </div>
                             <br>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="ipserver">SSID</label>
                                     <input class="col-lg-12 form-control jQKeyboard key" type="text" name="ssid" id="ssid">
                                 </div>
                                 <div class="col">
                                     <label class="jQKeyboard" for="ipaddress">PASSWORD</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="password" name="password" id="password">
                                 </div>
                             </div>
                             <hr>
                             <div class="form-check" style="margin:10px;">
                                 <input class="form-check-input" type="checkbox" id="enableIPWifi" name="enableIPWifi" value="1">
                                 <label class="jQKeyboard" for="enableIPLAN">Show IP Address Wifi</label>
                             </div>
                             <hr>
                             <button class="btn btn-success flex-fill font-weight-bold" id="btnsavewifi" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                 Save Wifi Configuration
                             </button>

                         </div>
                     </div>
                     <div class="tab-pane fade" id="system" role="tabpanel" aria-labelledby="system-tab">
                         <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                             <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                 <h6>System</h6>
                             </div>
                             <br>
                             <div class="row" style="padding:0px 10px">
                                 <div class="col">
                                     <label class="jQKeyboard" for="systemdelay">System Delay (S)</label>
                                     <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="systemdelay">
                                 </div>
                                 <div class="col">
                                     <button class="btn btn-success flex-fill font-weight-bold" id="btnsavedelay" style="margin:30px ; height:40px;"><i class="fas fa-check-circle"></i>
                                         Save
                                     </button>
                                 </div>

                             </div>
                             <br>
                             <div style="margin:10px" class="alert alert-info alert-dismissible fade show" role="alert">
                                 Press <strong>Delete Log</strong> button if you want to delete the entire log data.
                             </div>
                             <div style="margin:10px">
                                 <button class="btn btn-danger font-weight-bold" id="btn-del" style="width:100%;"> <i class="fas fa-trash"></i> &nbsp
                                     Delete Log
                                 </button>
                             </div>
                             <br>
                             <div style="margin:10px" class="alert alert-info alert-dismissible fade show" role="alert">
                                 Press <strong>Restart Device</strong> button if you want to reboot DAS Annunciator.
                             </div>
                             <div style="margin:10px">
                                 <button class="btn btn-primary font-weight-bold" id="btn-restart" style="width:100%;"> <i class="fa fa-refresh"></i> &nbsp
                                     Restart Device
                                 </button>
                             </div>

                             <hr>

                             <!-- <div class="d-flex justify-content-between flex-fill">
                                    <button class="btn btn-danger  flex-fill font-weight-bold" id="btncancel" style="height:40px;"> <i
                                            class="fas fa-chevron-circle-left"></i> &nbsp
                                        CANCEL
                                    </button>
                                    <button class="btn btn-success flex-fill font-weight-bold" id="btnsavewifi" style="height:40px;"><i class="fas fa-check-circle"></i>
                                        &nbsp
                                        SAVE
                                    </button>
                                </div> -->
                         </div>
                     </div>
                     <!-- </form> -->
                 </div>
             </div>

         </div>
     </div>
 </div>
 </div>

 <p hidden class="text-white" id="flg_set">home</p>
 <audio hidden controls id="alarm">
     <source src="../assets/sound/audio.wav" type="audio/wav">
 </audio>
 <div hidden id="key" style="z-index:2 ;
  left: 0%;right:0%; display: flex;
    justify-content: space-around;
    scale:2.5">
 </div>
 <p hidden id="port">0</p>
 <p hidden id="mute-config">0</p>
 <p hidden class="text-success" id="ack-config">0</p>
 <div class="fixed-bottom d-flex justify-content-between align-content-md-stretch bg-dark" style="padding-top:1px;padding-bottom:1px;">

     <a id="btn-home" class="flex-fill btn  text-center text-success" onclick="window.location.href='../beranda'">
         <i class="fas fa-home icon-nav" aria-hidden="true">&nbsp</i><br>
         HOME</a>

     <a id="btn-mute" class="flex-fill  btn text-center text-secondary" onclick="muteaudio()">
         <i class="fas fa-volume-up  icon-nav" aria-hidden="true" id="audio"></i><br>
         MUTE</a>

     <a id="btn-ack" class="flex-fill btn text-center text-secondary" onclick="ack()">
         <i class="fas fa-stop icon-nav" aria-hidden="true"></i>
         <br>ACK</a>


     <a class=" flex-fill btn text-center text-secondary" onclick="reset()">
         <i class="fa fa-sync-alt icon-nav" aria-hidden="true"></i>
         <br>RESET</a>

     <a class="flex-fill btn text-center text-secondary" onclick="gotosetting()" id="nav-config">
         <i class="fas fa-cog icon-nav" aria-hidden="true"></i>
         <br>CONFIG DISPLAY</a>


     <a class="flex-fill btn text-center text-secondary" onclick="gotolog()" id="nav-log">
         <i class="fas fa-address-book icon-nav" aria-hidden="true"></i>
         <br>LOG</a>


     <a class="flex-fill btn text-center text-secondary" onclick="activate(document.documentElement);" id="nav-exp">
         <i class="fas fa-expand icon-nav" aria-hidden="true"></i>
         <br>EXPAND</a>

     </ul>

 </div>


 <script>
     //first load
     $.ajax({
         method: "get",
         url: 'backend/webapi/viewsetting.php'
     }).done(function(msg) {
         let data = JSON.parse(msg)
         $('#code').val(data[0].sn)
         $('#bot_name').val(data[0].botname)
         $('#location').val(data[0].nama_gi)
         $('#name').val(data[0].nama)
         $('#port').val(data[0].displayed_port)
         $('#zWidth').val(data[0].zoom_w)
         $('#zHeight').val(data[0].zoom_h)
         $('#fontSize').val(data[0].f_size)
         $('#backlight').val(data[0].display_time)
         $('#pin').val(data[0].auth)
         $('#systemdelay').val(data[0].system_delay)

         $('#ipserver').val(data[0].ipserver)
         $('#ipaddress').val(data[0].iplocal)
         $('#netmask').val(data[0].netmask)
         $('#gateway').val(data[0].gateway)
         $('#dns').val(data[0].dns)


         if (data[0].dhcp == 1) {
             document.getElementById("dhcp").checked = true;
         }
         if (data[0].enableIPLAN == 1) {
             document.getElementById("enableIPLAN").checked = true;
         }
     })

     $.ajax({
         method: "get",
         url: '../api/display/admin'
     }).done(function(msg) {
         let data = (msg)
         // $('#port').val(data[0].total_port)
         $('#zoomH').val(data[0].zoom_h)
         $('#zoomW').val(data[0].zoom_w)
     })

     $.ajax({
         method: "get",
         url: '../api/wifi'
     }).done(function(msg) {
         let data = (msg)
         $('#ssid').val(data[0].ssid)
         $('#password').val(data[0].pass)
         if (data[0].enableIPWifi == 1) {
             document.getElementById("enableIPWifi").checked = true;
         }
     })

     //keyboard service
     $('#ipserver').click(function() {

         $('#key').show()
         $('#key').removeClass('pos2')
         $('#key').removeClass('pos3')
         $('#key').removeClass('pos4')
         $('#key').removeClass('pos5')
         $('#key').addClass('pos1')
     })
     $('#ipaddress').click(function() {
         $('#key').show()
         $('#key').removeClass('pos1')
         $('#key').removeClass('pos3')
         $('#key').removeClass('pos4')
         $('#key').removeClass('pos5')
         $('#key').addClass('pos2')
     })
     $('#netmask').click(function() {
         $('#key').show()
         $('#key').removeClass('pos1')
         $('#key').removeClass('pos2')
         $('#key').removeClass('pos4')
         $('#key').removeClass('pos5')
         $('#key').addClass('pos3')
     })
     $('#gateway').click(function() {
         $('#key').show()
         $('#key').removeClass('pos1')
         $('#key').removeClass('pos2')
         $('#key').removeClass('pos3')
         $('#key').removeClass('pos5')
         $('#key').addClass('pos4')
     })
     $('#dns').click(function() {
         $('#key').show()
         $('#key').removeClass('pos1')
         $('#key').removeClass('pos2')
         $('#key').removeClass('pos3')
         $('#key').removeClass('pos4')
         $('#key').addClass('pos5')
     })

     var keyboard = {
         'layout': [
             // alphanumeric keyboard type
             // text displayed on keyboard button, keyboard value, keycode, column span, new row
             [
                 [
                     ['`', '`', 192, 0, true],
                     ['1', '1', 49, 0, false],
                     ['2', '2', 50, 0, false],
                     ['3', '3', 51, 0, false],
                     ['4', '4', 52, 0, false],
                     ['5', '5', 53, 0, false],
                     ['6', '6', 54, 0, false],
                     ['7', '7', 55, 0, false],
                     ['8', '8', 56, 0, false],
                     ['9', '9', 57, 0, false],
                     ['0', '0', 48, 0, false],
                     ['-', '-', 189, 0, false],
                     ['=', '=', 187, 0, false],
                     ['q', 'q', 81, 0, true],
                     ['w', 'w', 87, 0, false],
                     ['e', 'e', 69, 0, false],
                     ['r', 'r', 82, 0, false],
                     ['t', 't', 84, 0, false],
                     ['y', 'y', 89, 0, false],
                     ['u', 'u', 85, 0, false],
                     ['i', 'i', 73, 0, false],
                     ['o', 'o', 79, 0, false],
                     ['p', 'p', 80, 0, false],
                     ['[', '[', 219, 0, false],
                     [']', ']', 221, 0, false],
                     ['&#92;', '\\', 220, 0, false],
                     ['a', 'a', 65, 0, true],
                     ['s', 's', 83, 0, false],
                     ['d', 'd', 68, 0, false],
                     ['f', 'f', 70, 0, false],
                     ['g', 'g', 71, 0, false],
                     ['h', 'h', 72, 0, false],
                     ['j', 'j', 74, 0, false],
                     ['k', 'k', 75, 0, false],
                     ['l', 'l', 76, 0, false],
                     [';', ';', 186, 0, false],
                     ['&#39;', '\'', 222, 0, false],
                     ['Enter', '13', 13, 3, false],
                     ['Shift', '16', 16, 2, true],
                     ['z', 'z', 90, 0, false],
                     ['x', 'x', 88, 0, false],
                     ['c', 'c', 67, 0, false],
                     ['v', 'v', 86, 0, false],
                     ['b', 'b', 66, 0, false],
                     ['n', 'n', 78, 0, false],
                     ['m', 'm', 77, 0, false],
                     [',', ',', 188, 0, false],
                     ['.', '.', 190, 0, false],
                     ['/', '/', 191, 0, false],
                     ['Shift', '16', 16, 2, false],
                     ['Bksp', '8', 8, 3, true],
                     ['Space', '32', 32, 12, false],
                     ['Clear', '46', 46, 3, false],
                     ['Cancel', '27', 27, 3, false]
                 ]
             ]
         ]
     }
     $('input.jQKeyboard').initKeypad({
         'keyboardLayout': keyboard
     });

     $('#btn-del').click(function() {
         $("#valmodal").modal('show')
         $("#text-config").text("Do you want to delete data?")
         $("#conf-text").text("del")
     });
     $('#btn-restart').click(function() {
         $("#valmodal").modal('show')
         $("#text-config").text("Do you want to Restart device?")
         $("#conf-text").text("rest")
     });
     $('#btn-yes').click(function() {
         let conf = $("#conf-text").text()
         if (conf == "del") {
             deletedevice();
         } else if (conf == "rest") {
             restartdevice()
         }
     });
     $('#btn-no').click(function() {
         $("#valmodal").modal('hide')
     })

     function deletedevice() {
         $.ajax({
             method: "get",
             url: 'backend/webapi/deletelog.php'
         }).done(function() {
             Swal.fire({
                 position: 'top-center',
                 icon: 'success',
                 title: 'Your data has been deleted',
                 showConfirmButton: false,
                 timer: 1500
             }).then(function() {
                 $("#valmodal").modal('hide')
             })

         })
     }

     function restartdevice() {
         $.ajax({
             method: "get",
             url: 'backend/webapi/reboot.php'
         }).done(function() {
             Swal.fire({
                 position: 'top-center',
                 icon: 'success',
                 title: 'device is restarted',
                 showConfirmButton: false,
                 timer: 1500
             }).then(function() {
                 $("#valmodal").modal('hide')
             })

         })
     }
     $('#btncancel').click(function() {
         gotohome()
     });

     $('#btnsaveajah').click(function() {
         let port = $('#port').val();
         let zoomW = $('#zoomW').val();
         let zoomH = $('#zoomH').val();
         $.ajax({
             method: "post",
             // headers: {'Content-Type': 'application/json'}, 
             // body: JSON.stringify(data),
             url: 'api/params/admin',
             data: {
                 total_port: port,
                 zoom_w: zoomW,
                 zoom_h: zoomH,

             }
         }).done(function(msg) {

             let data = (msg)
             if (data.code == 1) {
                 Swal.fire({
                     position: 'top-center',
                     icon: 'success',
                     title: 'Your data has been saved',
                     showConfirmButton: false,
                     timer: 1500
                 }).then(function() {
                     gotohome()
                 })
             } else {
                 alert(msg)
             }
             // alert('update success')
         })
     });

     $('#btnsavewifi').click(function() {
         let ssid = $('#ssid').val();
         let password = $('#password').val();
         let enableIPWifi = $('#enableIPWifi').is(':checked') ? $('#enableIPWifi').val() : 0;
         let flag = 1;
         $.ajax({
             method: "post",
             // headers: {'Content-Type': 'application/json'}, 
             // body: JSON.stringify(data),
             url: '../api/wifi/1',
             data: {
                 ssid: ssid,
                 pass: password,
                 enableIPWifi: enableIPWifi,
                 flag: flag,

             }
         }).done(function(msg) {

             let data = (msg)
             if (data.code == 1) {
                 Swal.fire({
                     position: 'top-center',
                     icon: 'success',
                     title: 'Wifi setting has been change',
                     showConfirmButton: false,
                     timer: 1500
                 }).then(function() {
                     gotohome()
                 })
             } else {
                 alert(msg)
             }
             // alert('update success')
         })
     });

     $('#btnsavedelay').click(function() {
         let systemdelay = $('#systemdelay').val();
         $.ajax({
             method: "post",
             // headers: {'Content-Type': 'application/json'}, 
             // body: JSON.stringify(data),
             url: '../api/delay/1',
             data: {
                 system_delay: systemdelay,
             }
         }).done(function(msg) {

             let data = (msg)
             if (data.code == 1) {
                 Swal.fire({
                     position: 'top-center',
                     icon: 'success',
                     title: 'System delay has been change',
                     showConfirmButton: false,
                     timer: 1500
                 }).then(function() {
                     gotohome()
                 })
             } else {
                 alert(msg)
             }
             // alert('update success')
         })
     });

     $('#btnsavelan').click(function() {
         let ipserver = $('#ipserver').val();
         let ipaddress = $('#ipaddress').val();
         let netmask = $('#netmask').val();
         let gateway = $('#gateway').val();
         let dns = $('#dns').val();
         let loc = $('#location').val();
         let name = $('#name').val();
         let port = $('#port').val();
         let zoomW = $('#zWidth').val();
         let zoomH = $('#zHeight').val();
         let fontSize = $('#fontSize').val();
         let backlight = $('#backlight').val();
         let auth = $('#pin').val();
         let dhcp = $('#dhcp').is(':checked') ? $('#dhcp').val() : 0;
         let enableIPLAN = $('#enableIPLAN').is(':checked') ? $('#enableIPLAN').val() : 0;
         $.ajax({
             method: "post",
             url: 'backend/webapi/updatesetting.php',
             data: {
                 ipserver: ipserver,
                 ipaddr: ipaddress,
                 netmask: netmask,
                 gateway: gateway,
                 dns: dns,
                 loc: loc,
                 zoom_w: zoomW,
                 zoom_h: zoomH,
                 name: name,
                 port: port,
                 f_size: fontSize,
                 display_time: backlight,
                 auth: auth,
                 dhcp: dhcp,
                 enableIPLAN: enableIPLAN,

             }
         }).done(function(msg) {

             let data = JSON.parse(msg)
             if (data.code == 1) {
                 Swal.fire({
                     position: 'top-center',
                     icon: 'success',
                     title: 'Your data has been saved',
                     showConfirmButton: false,
                     timer: 1500
                 }).then(function() {
                     gotohome()
                 })
             } else {
                 alert(msg)
             }
             // alert('update success')
         })
     });

     $('#btnsave').click(function() {
         let ipserver = $('#ipserver').val();
         let ipaddress = $('#ipaddress').val();
         let netmask = $('#netmask').val();
         let gateway = $('#gateway').val();
         let dns = $('#dns').val();
         let loc = $('#location').val();
         let name = $('#name').val();
         let port = $('#port').val();
         let zoomW = $('#zWidth').val();
         let zoomH = $('#zHeight').val();
         let fontSize = $('#fontSize').val();
         let backlight = $('#backlight').val();
         let auth = $('#pin').val();
         let dhcp = $('#dhcp').is(':checked') ? $('#dhcp').val() : 0;
         let enableIPLAN = $('#enableIPLAN').is(':checked') ? $('#enableIPLAN').val() : 0;

         $.ajax({
             method: "post",
             url: 'backend/webapi/updatesetting.php',
             data: {
                 ipserver: ipserver,
                 ipaddr: ipaddress,
                 netmask: netmask,
                 gateway: gateway,
                 dns: dns,
                 loc: loc,
                 name: name,
                 port: port,
                 zoom_w: zoomW,
                 zoom_h: zoomH,
                 f_size: fontSize,
                 dhcp: dhcp,
                 display_time: backlight,
                 auth: auth,
                 enableIPLAN: enableIPLAN,

             }
         }).done(function(msg) {

             let data = JSON.parse(msg)
             if (data.code == 1) {
                 Swal.fire({
                     position: 'top-center',
                     icon: 'success',
                     title: 'Your data has been saved',
                     showConfirmButton: false,
                     timer: 1500
                 }).then(function() {
                     gotohome()
                 })
             } else {
                 alert(msg)
             }
             // alert('update success')
         })
     });
 </script>