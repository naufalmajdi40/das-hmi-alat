<style></style>
<div class="d-flex flex-wrap" id="das-container" style="width: 100%">
	<!-- <div class="kotak bg-red btn text-left">
        <h7 style="margin-left:5px" class="text-primary">
            <b>ANOUNCER</b></h7>
        <br>
        <h7 style="margin-left:5px;margin-top:-5px" class="text-white">TRIP</h7>
    </div> -->
</div>
<script>

    var ports=0;
    $.ajax({
        method: "get",
        url: 'backend/webapi/viewsetting.php'
    }).done(function (msg) {
		let data = JSON.parse(msg)
        ports = data[0].displayed_port;

		$.ajax({
		method: "get",
		url: "/../../das/api/item/" + ports,
	}).done(function (msg) {
		//console.log(msg[1]);
		let data = msg;
		for (let i = 0; i < data.length; i++) {
			var kondisi = "";

			let classStatus = "";
			if (data[i].flag_kondisi == "1") {
				kondisi = data[i].high;
				classStatus = "bg-red";
			} else {
				kondisi = data[i].low;
				classStatus = "bg-green";
			}
			$("#das-container").append(`
                 <div style="position:relative;" class="kotak ${classStatus} btn text-left mr-auto" id ="box-${data[i].port}" onclick="gotodetail('${data[i].port}')">
                   
					<span style="color:#d7f172; position:absolute; margin-top:5px;margin-left: -10px; z-index:10"  class="badge ">${data[i].port}</span>
                    <p style="position:relative; letter-spacing: 0.05px; line-height: 1; margin-left:15px;margin-top:23px ;margin-bottom:-10px" class="text-light text-center">
                    <b>${data[i].nama_alat}</b></p>
                <br>
                <p hidden style="margin-left:30px;margin-top:-10px;" class="text-light" id=txt-kondisi-${data[i].port}> ${kondisi}</p > <br>
            <p hidden style=" float: right;margin-top:-40px; margin-bottom:10px" class="text-light" > Port :${data[i].port}</p>      
                </div>`);
		}
	
	});
    });



	
	const loopKondisi = setInterval(readKondisi, 1000);

	function readKondisi() {
		$.ajax({
			method: "get",
			url: "../../das/api/item",
		}).done(function (msg) {
			let data = msg;
			let ack = $("#ack-config").text();
			for (let i = 0; i < data.length; i++) {
				let classStatus = "";

				if (data[i].flag_kondisi == "1" && ack == 0) {
					$("#box-" + data[i].port).removeClass("bg-green");
					$("#box-" + data[i].port).removeClass("bg-red-ack");
					$("#box-" + data[i].port).addClass("bg-red");
					$("#txt-kondisi-" + data[i].port).text(data[i].high);
				} else if (data[i].flag_kondisi == "1" && ack == 1) {
					$("#box-" + data[i].port).removeClass("bg-green");
					$("#box-" + data[i].port).removeClass("bg-red");
					$("#box-" + data[i].port).addClass("bg-red-ack");
					$("#txt-kondisi-" + data[i].port).text(data[i].high);
				} else {
					$("#box-" + data[i].port).removeClass("bg-red");
					$("#box-" + data[i].port).removeClass("bg-red-ack");
					$("#box-" + data[i].port).addClass("bg-green");
					$("#txt-kondisi-" + data[i].port).text(data[i].low);
				}
			}
		});
	}
	function stoploop() {
		clearInterval(loopKondisi);
	}
	function gotodetail(x) {
		// alert(x)
		$("#port").text(x);
		$("#main").load("main/channel.html");
	}
	function activate(ele) {
		if (ele.requestFullscreen) {
			ele.requestFullscreen();
			document.getElementById(full).style.display = "hidden";
		}
	}
</script>
