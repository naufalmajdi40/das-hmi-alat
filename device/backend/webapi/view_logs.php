<?php 
require_once '../koneksi.php';
$mysqli = new mysqli($host, $user, $pass, $db);
if($_GET['action'] == "table_data"){


		$columns = array( 
                                0 =>'id', 
                                1 =>'location', 
	                            2 =>'name',
	                            3=> 'channel',
	                            4=> 'status',
								5=> 'date',
	                        );

		$querycount = $mysqli->query("SELECT count(id) as jumlah FROM notif_list");
		$datacount = $querycount->fetch_array();
	
  
        $totalData = $datacount['jumlah'];
            
        $totalFiltered = $totalData; 

        $limit = $_POST['length'];
        $start = $_POST['start'];
        $order = $columns[$_POST['order']['0']['column']];
        $dir = $_POST['order']['0']['dir'];
            
        if(empty($_POST['search']['value']))
        {            
        	$query = $mysqli->query("SELECT notif_list.id,
                                            notif_list.nama_gi as location,
                                            notif_list.nama_alat as nama,
                                            notif_list.port as channel ,
                                             notif_list.status,
                                             notif_list.tanggal as date
																		FROM notif_list
                                                                        left join m_mesin 
                                                                        on m_mesin.kode_mesin = notif_list.kode_mesin
                                                                        order by $order $dir 
        																LIMIT $limit 
        																OFFSET $start");
        }
        else {
            $search = $_POST['search']['value']; 
            $query = $mysqli->query("SELECT notif_list.id,
                                            notif_list.nama_gi as location,
                                            notif_list.nama_alat as nama,
                                            notif_list.port as channel ,
                                            notif_list.status, 
                                            notif_list.tanggal as date
																		FROM notif_list
                                                                        left join m_mesin 
                                                                        on m_mesin.kode_mesin = notif_list.kode_mesin
                                                                       
																		WHERE notif_list.nama_alat LIKE '%$search%' 
            															or notif_list.port LIKE '%$search%' 
            															order by $order $dir 
            															LIMIT $limit 
            															OFFSET $start");


           $querycount = $mysqli->query("SELECT count(id) as jumlah FROM notif_list WHERE nama_alat LIKE '%$search%' 
       																						or port LIKE '%$search%'");
		   $datacount = $querycount->fetch_array();
           $totalFiltered = $datacount['jumlah'];
        }

        $data = array();
        if(!empty($query))
        {
            $no = $start + 1;
            while ($r = $query->fetch_array())
            {
                $nestedData['no'] = $no;
                $nestedData['location'] = $r['location'];
                $nestedData['nama'] = $r['nama'];
                $nestedData['channel'] = $r['channel'];
                $nestedData['status'] = $r['status'];
				$nestedData['date'] = $r['date'];
                $data[] = $nestedData;
                $no++;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($_POST['draw']),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 

}