<div class="container" style="padding-top: 80px;">
  <div class="row">
    <div class="col-lg-12">
      <button id="btnexcel" class="invisible btn btn-success" onclick="javascript:location.href='backend/webapi/export.php'">
        <i class="fas fa-file-excel" style="font-size: 20px;">
        </i>&nbsp; Export to Excel
      </button>
      <button id="btnpdf" class="invisible btn btn-danger" onclick="exportpdf()">
        <i class="fas fa-file-pdf" style="font-size: 20px"></i>&nbsp; Export to PDF
      </button>

      <div style="margin: 20px 0px;"></div>

      <div class="table-responsive table-dark col-md-12 border border-dark">
        <table class="table table-striped table-dark table-hover table-sm">
          <thead class="thead-success">
            <tr class="text-white">
              <th scope="col">No</th>
              <th scope="col">Location</th>
              <th scope="col">Namesss</th>
              <th scope="col">Channel</th>
              <th scope="col">Status</th>
              <th scope="col">Date</th>
            </tr>
          </thead>
          <tbody>
            <!-- List Data Menggunakan DataTable -->
          </tbody>
        </table>
      </div>

    </div>

  </div>
</div>
<script>
  let host = window.location.hostname;
  if (host != "localhost") {
    $("#btnexcel").removeClass("invisible");
    $("#btnpdf").removeClass("invisible");
  }

  function exportpdf() {
    $.ajax({
      method: "get",
      url: "backend/webapi/viewlog.php",
    }).done(function(msg) {
      //alert(msg)
      let data = JSON.parse(msg);
      var rows = [];
      for (let i = 0; i < data.length; i++) {
        rows.push([
          i,
          data[i]["nama_gi"],
          data[i]["nama_alat"],
          data[i]["port"],
          data[i]["status"],
          data[i]["tanggal"],
        ]);
      }
      var doc = new jsPDF("l", "pt");
      var headers = ["No", "Location", "Name", "Channel", "Status", "Date"];
      doc.autoTable(headers, rows);
      doc.save("export-" + getdate());
    });
  }
  $(function() {
    $(".table").DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: "backend/webapi/view_logs.php?action=table_data",
        dataType: "json",
        type: "POST",
      },
      columns: [{
          data: "no"
        },
        {
          data: "location"
        },
        {
          data: "nama"
        },
        {
          data: "channel"
        },
        {
          data: "status"
        },
        {
          data: "date"
        },
      ],
    });
  });
</script>
<script>
  $(function() {
    $(this).bind("contextmenu", function(e) {
      e.preventDefault();
    });
  });
</script>