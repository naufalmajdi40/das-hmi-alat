<?php
$user = $_GET['u'];
$level = $_GET['l'];

?>
<div>
    <br>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="container d-flex justify-content-center">
            <div class="col-sm-12">
                <div class="mb-6">
                    <ul class="nav nav-pills" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="devicesetting-tab" data-toggle="tab" href="#deviceSetting" role="tab" aria-controls="home" aria-selected="true">Device Setting</a></li>
                        <li class="nav-item"><a class="nav-link" id="lansetting-tab" data-toggle="tab" href="#lanSetting" role="tab" aria-controls="profile" aria-selected="false">LAN Setting</a></li>
                        <li class="nav-item"><a class="nav-link" id="wifisetting-tab" data-toggle="tab" href="#wifiSetting" role="tab" aria-controls="profile" aria-selected="false">Wifi Setting</a></li>
                        <li class="nav-item"><a class="nav-link" id="display-tab" data-toggle="tab" href="#displaySetting" role="tab" aria-controls="contact" aria-selected="false">Display Setting</a></li>
                        <li class="nav-item"><a class="nav-link" id="system-tab" data-toggle="tab" href="#systemSetting" role="tab" aria-controls="contact" aria-selected="false">System</a></li>
                        <li class="nav-item"><a class="nav-link" id="p_meter-tab" data-toggle="tab" href="#p_meterSetting" role="tab" aria-controls="contact" aria-selected="false">Power Meter</a></li>
                        <li class="nav-item"><a class="nav-link" id="p_meter-tab" data-toggle="tab" href="#teleSetting" role="tab" aria-controls="contact" aria-selected="false">Telegram</a></li> 
                        <li class="nav-item"><a class="nav-link" id="p_meter-tab" data-toggle="tab" href="#param_setting" role="tab" aria-controls="contact" aria-selected="false" onClick="getParam()">Unit Setting</a></li> 
                       
                        <li class="nav-item"><a class="nav-link" id="myaccount-tab" data-toggle="tab" href="#myAccount" role="tab" aria-controls="contact" aria-selected="false">My Account</a></li>
                        <?php if ($level == 'superadmin') {  ?>
                            <li class="nav-item"><a class="nav-link" id="allusers-tab" data-toggle="tab" href="#allUsers" role="tab" aria-controls="contact" aria-selected="false">All Users</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="tab-content mb-6" style="overflow-y: auto;height:720px">
                    <div class="tab-pane fade show active" id="deviceSetting" role="tabpanel" aria-labelledby="devicesetting-tab">
                        <div class="my-3 text-white " style="background-color:#000000;border-radius: 5px;">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>Device Setting</h6>
                            </div>
                            <br>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="code">Device Code</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="" id="code" disabled>

                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="location">Bay Name</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="bay_name">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="location">Location</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="location">
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="name">Device Name</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="name">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="bot_name">Bay Code</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="bay_code" >
                                </div>
                                <div hidden class="col">
                                    <label class="jQKeyboard" for="bot_name">Bot Name</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="bot_name" disabled>
                                </div>

                            </div>
                            <br>

                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsave" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                            <button class="btn btn-danger flex-fill font-weight-bold col-md-3" id="sync" onclick="sync()" style="margin:10px ; height:40px;"><i class="fas fa-sync-alt"></i>
                                SYNC DATA
                            </button>
                            <hr>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="lanSetting" role="tabpanel" aria-labelledby="lansetting-tab">
                        <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>LAN Setting</h6>
                            </div>
                            <div class="form-check" style="margin:10px;">
                                <input class="form-check-input" type="checkbox" id="dhcp" name="dhcp" value="1">
                                <label class="jQKeyboard" for="ipserver">Enable DHCP</label>
                            </div>
                            <div class="row" style="padding:0px 10px">
                                
                                <div class="col">
                                    <label class="jQKeyboard" for="ipserver">IP SERVER GIPAT</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="" id="ipgipat">
                                    <div id="ipgipat"></div>
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="ipserver">IP SERVER</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="" id="ipserver">
                                    <div id="ip"></div>
                                </div>
                               
                                <div class="col">
                                    <label class="jQKeyboard" for="ipaddress">IP ADDRESS</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="ipaddress">
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="netmask">NETMASK</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="netmask">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="gateway">GATEWAY</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="gateway">
                                </div>
                            </div>
                            <hr>
                            <div style="margin:10px">
                                <label class="jQKeyboard" for="dns">DNS</label>
                                <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="dns">
                            </div>
                            <hr>
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsavelan" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="wifiSetting" role="tabpanel" aria-labelledby="wifisetting-tab">
                        <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>Wifi Setting</h6>
                            </div>
                            <br>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="ipserver">SSID</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="ssid" id="ssid">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="ipaddress">PASSWORD</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="password" name="password" id="password">
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsavewifi" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="displaySetting" role="tabpanel" aria-labelledby="display-tab">
                        <!-- <form action="<?php echo $action; ?>" method="post"> -->
                        <div class="my-3 text-white " style="background-color:#000000 ;border-radius: 5px; ">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>Display Setting</h6>
                            </div>
                            <br>

                            <div style="margin:10px; color:#00dcff">
                                <p>This setting only affects the user, not on the screen installed on the panel.</p>
                            </div>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="code">Displayed Port</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="number" name="port" id="port">
                                    <div id="ip"></div>
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="code">Font Size</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="number" name="fontSize" id="fontSize">
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="Zoom Width">Zoom Width</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="zoomW" id="zoomW">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="Zoom Height">Zoom Height</label>
                                    <input class="col-lg-12 form-control" type="text" name="zoomH" id="zoomH">

                                </div>

                            </div>
                            <hr>
                            <!-- <div class="col-3" > -->
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsaveajah" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                            <!-- <button style="margin:10px" class="btn btn-primary font-weight-bold" id="btnsaveajah">
                                Save Setting
                            </button> -->
                            <!-- </div> -->
                            <hr>
                        </div>
                        <!-- </form> -->
                    </div>
                    

                    <div class="tab-pane fade" id="param_setting" role="tabpanel" aria-labelledby="system-tab">
                        <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>Parameter Setting</h6>
                            </div>
                            <br>
                            <p hidden id="param_length">0</p>
                            <div class="row" id="param_body" style="padding:0px 10px">
                                <!-- <div class="col">
                                    <label class="jQKeyboard" for="chat">CHAT ID</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="chatid" id="chatid">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="api">API KEY</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="password" name="api" id="api">
                                </div> -->
                            </div>
                            
                            
                            
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsaveparam" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                            <hr>
                            <hr>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade" id="teleSetting" role="tabpanel" aria-labelledby="system-tab">
                        <div class="my-3 text-white " style="background-color:#000000; border-radius: 5px;">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>Telegram Setting</h6>
                            </div>
                            <br>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="chat">CHAT ID</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="chatid" id="chatid">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="api">API KEY</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="password" name="api" id="api">
                                </div>
                            </div>
                            <hr>
                            
                            <button class="btn btn-warning flex-fill font-weight-bold" id="btngetchatid" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                GET CHATID
                            </button>
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsavetele" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="systemSetting" role="tabpanel" aria-labelledby="system-tab">

                        <div class="my-3 text-white " style="background-color:#000000 ;border-radius: 5px; ">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>System</h6>
                            </div>
                            <br>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="systemdelay">System Delay (S)</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="text" name="" id="systemdelay">
                                </div>
                                <div class="col">
                                    <button class="btn btn-success flex-fill font-weight-bold" id="btnsavedelay" style="margin:30px ; height:40px;"><i class="fas fa-check-circle"></i>
                                        Save
                                    </button>
                                </div>

                            </div>
                            <br>
                            <div style="margin:10px" class="alert alert-info alert-dismissible fade show" role="alert">
                                Press <strong>Delete Log</strong> button if you want to delete the entire log data.
                            </div>
                            <div style="margin:10px">
                                <button class="btn btn-danger font-weight-bold" id="btn-del" style="width:100%;"> <i class="fas fa-trash"></i> &nbsp
                                    Delete Log
                                </button>
                            </div>
                            <br>
                            <div style="margin:10px" class="alert alert-info alert-dismissible fade show" role="alert">
                                Press <strong>Restart Device</strong> button if you want to reboot DAS Annunciator.
                            </div>
                            <div style="margin:10px">
                                <button class="btn btn-primary font-weight-bold" id="btn-restart" style="width:100%;"> <i class="fa fa-refresh"></i> &nbsp
                                    Restart Device
                                </button>
                            </div>
                            <hr>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="p_meterSetting" role="tabpanel" aria-labelledby="p_meter-tab" >

                        <div class="my-3 text-white " style="background-color:#000000 ;border-radius: 5px; ">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>Power Meter</h6>
                            </div>
                            <br>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Power Meter Type</label>
                                    <select class="custom-select mr-sm-2" name="metertype" id="metertype">
                                        <option value="0">Acuavim II</option>
                                        <option value="1">Schneider DM6000H</option>
                                         <option value="2">SIEMENS 7KT0310</option>
                                       
                                    </select>
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="code">ID Device</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="number" name="idmeter" id="idmeter">
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Baudrate</label>
                                    <select class="custom-select mr-sm-2" name="baudrate" id="baudrate">
                                        <option value="600">600</option>
                                        <option value="1200">1200</option>
                                        <option value="2400">2400</option>
                                        <option value="4800">4800</option>
                                        <option value="9600">9600</option>
                                        <option value="14400">14400</option>
                                        <option value="19200">19200</option>
                                        <option value="56000">56000</option>
                                        <option value="57600">57600</option>
                                        <option value="115200">115200</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Parity</label>
                                    <select class="custom-select mr-sm-2" name="parity" id="parity">
                                        <option value="0">None</option>
                                        <option value="1">Even</option>
                                        <option value="2">Odd</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Data Bits</label>
                                    <select class="custom-select mr-sm-2" name="databits" id="databits">
                                        <option value="8">8 Bits</option>
                                        <option value="7">7 Bits</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Stop Bits</label>
                                    <select class="custom-select mr-sm-2" name="stopbits" id="stopbits">
                                        <option value="1">1 Bit</option>
                                        <option value="2">2 Bits</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Mqtt server</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="mqttserver" id="mqttserver">
                              
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="code">Mqtt port</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="number" name="mqttport" id="mqttport">
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">MQTT username</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="mqttuser" id="mqttuser">
                              
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="code">MQTT Password</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="password" name="mqttpass" id="mqttpass">
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsavemeter" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="myAccount" role="tabpanel" aria-labelledby="myaccount-tab">

                        <div class="my-3 text-white " style="background-color:#000000 ;border-radius: 5px; ">
                            <div class="col-lg-12 " style="background-color:#1182bfb5 ;border-radius: 5px; padding : 5px 10px ">
                                <h6>My Account</h6>
                            </div>
                            <br>
                            <div class="row" style="padding:0px 10px">
                                <div class="col">
                                    <label class="jQKeyboard" for="ipserver">USERNAME</label>
                                    <input class="col-lg-12 form-control jQKeyboard key" type="text" name="username" id="username">
                                </div>
                                <div class="col">
                                    <label class="jQKeyboard" for="ipaddress">PASSWORD</label>
                                    <input class="col-lg-12 form-control jQKeyboard" type="password" name="passworduser" id="passworduser">
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-success flex-fill font-weight-bold" id="btnsaveaccount" style="margin:10px ; height:40px;"><i class="fas fa-check-circle"></i>
                                SAVE CONFIGURATION
                            </button>
                            <hr>
                        </div>

                    </div>
                    
                    <div class="tab-pane fade" id="allUsers" role="tabpanel" aria-labelledby="allusers-tab">

                        <div class="my-3 text-white " style="background-color:#000000 ;border-radius: 5px; ">
                            <!-- <div class="row"> -->
                            <!-- <div class="col-lg-12"> -->


                            <!-- <div style="margin: 20px 0px;"></div> -->
                            <button type="button" class="btn btn-primary text-white" data-toggle="modal" data-target="#addUsers">
                                Add New User
                            </button>
                            <hr>

                            <div class="table-responsive table-dark col-md-12 border border-dark">

                                <table width="100%" id="tableUser" class="table  table-striped table-dark border-dark  ">
                                    <thead class="thead-success">
                                        <tr class="text-white">
                                            <!-- <th scope="col">No</th> -->
                                            <th scope="col">Username</th>
                                            <th scope="col">Nama Lengkap</th>
                                            <th scope="col">Level</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- List Data Menggunakan DataTable -->
                                    </tbody>
                                </table>
                            </div>

                            <!-- </div> -->

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
</div>

<!-- Form Add Users -->
<div class="modal fade" id="addUsers" tabindex="-1" role="dialog" aria-labelledby="addUsers" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Users</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <label for="nama_lengkap">Nama Lengkap</label>
                    <input class="col-lg-12 form-control" type="text" name="namaLengkap" id="namaLengkap">
                </div>
                <hr>
                <div>
                    <label for="addusername">Username</label>
                    <input class="col-lg-12 form-control" type="text" name="addUsername" id="addUsername">
                </div>
                <hr>
                <div>
                    <label for="addpassword">Password</label>
                    <input class="col-lg-12 form-control" type="text" name="addPassword" id="addPassword">
                </div>
                <hr>
                <div>
                    <label for="addlevel">Level</label>
                    <select name="addlevel" id="addlevel" class="col-sm-4 form-control">
                        <option value="admin" selected="selected">Admin</option>
                        <option value="superadmin">Super Admin</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btnadduser" class="btn btn-primary">Add User</button>
            </div>
        </div>
    </div>
</div>
<!-- End Form Edit -->

<!-- Form Add Users -->
<div class="modal fade" id="userUpdate" tabindex="-1" role="dialog" aria-labelledby="userUpdate" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userUpdate">Users Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <label for="nama_lengkap">Nama Lengkap</label>
                    <input class="col-lg-12 form-control" type="text" name="namaLengkap" id="namaLengkap">
                </div>
                <hr>
                <div>
                    <label for="addusername">Username</label>
                    <input class="col-lg-12 form-control" type="text" name="addUsername" id="addUsername">
                </div>
                <hr>
                <div>
                    <label for="addpassword">Password</label>
                    <input class="col-lg-12 form-control" type="text" name="addPassword" id="addPassword">
                </div>
                <hr>
                <div>
                    <label for="addlevel">Level</label>
                    <select name="addlevel" id="addlevel" class="col-sm-4 form-control">
                        <option value="admin" selected="selected">Admin</option>
                        <option value="superadmin">Super Admin</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btnadduser" class="btn btn-primary">Add User</button>
            </div>
        </div>
    </div>
</div>
<!-- End Form Edit -->

<p hidden class="text-white" id="flg_set">home</p>
<audio hidden controls id="alarm">
    <source src="assets/sound/audio.wav" type="audio/wav">
</audio>
<div hidden id="key" style="z-index:2 ;
  left: 0%;right:0%; display: flex;
    justify-content: space-around;
    scale:2.5">
</div>
<p hidden id="port">0</p>
<p hidden id="mute-config">0</p>
<p hidden class="text-success" id="ack-config">0</p>
<div class="fixed-bottom d-flex justify-content-between align-content-md-stretch bg-dark" style="padding-top:1px;padding-bottom:1px;">

    <a id="btn-home" class="flex-fill btn  text-center text-success" onclick="window.location.href='../beranda'">
        <i class="fas fa-home icon-nav" aria-hidden="true">&nbsp</i><br>
        HOME</a>

    <a id="btn-mute" class="flex-fill  btn text-center text-secondary" onclick="muteaudio()">
        <i class="fas fa-volume-up  icon-nav" aria-hidden="true" id="audio"></i><br>
        MUTE</a>

    <a id="btn-ack" class="flex-fill btn text-center text-secondary" onclick="ack()">
        <i class="fas fa-stop icon-nav" aria-hidden="true"></i>
        <br>ACK</a>


    <a class=" flex-fill btn text-center text-secondary" onclick="reset()">
        <i class="fa fa-sync-alt icon-nav" aria-hidden="true"></i>
        <br>RESET</a>

    <a class="flex-fill btn text-center text-secondary" onclick="gotosetting()" id="nav-config">
        <i class="fas fa-cog icon-nav" aria-hidden="true"></i>
        <br>CONFIG DISPLAY</a>


    <a class="flex-fill btn text-center text-secondary" onclick="gotolog()" id="nav-log">
        <i class="fas fa-address-book icon-nav" aria-hidden="true"></i>
        <br>LOG</a>


    <a class="flex-fill btn text-center text-secondary" onclick="activate(document.documentElement);" id="nav-exp">
        <i class="fas fa-expand icon-nav" aria-hidden="true"></i>
        <br>EXPAND</a>

    </ul>

</div>


<script>
    //first load
    function sync(){
        sync2()
        .then((data) => {
            //console.log(data)
            sync3(data)
        })
        .catch((error) => {
            console.log(error)
        })
    }
    function sync2() {
        return new Promise((resolve, reject) => {
            $.ajax({
            url: "api/Power_param",
            type: 'GET',
            data: {
                key: 'value',
            },
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
            })
        })
    }
   
    function sync3(power_param){
        $.get('backend/webapi/viewchannel.php', function(dta) {
        let dt = JSON.parse(dta)
       // alert(power_param[0].param_id)
        let var_power = [];
        let var_name =[];
        let var_divider=[];
        let unit=[]
        for (let i =0;i<power_param.length;i++){
            var_power.push(power_param[i].param_id)
            var_name.push(power_param[i].param_name)
            var_divider.push(power_param[i].divider)
            unit.push(power_param[i].param_unit)
            console.log(power_param[i].param_id)
        }
      
        // let var_power=["VRN","VSN","VTN","VAVGN","VRS","VST","VRT","VAVG","IR","IS","IT","IAVG","PFAVG","F"]
        // let var_name=["Volt R-N","Volt S-N","Volt T-N","Volt R-S-T-N","Volt R-S","Volt S-T","Volt R-T","Volt R-S-T","Phase R","Phase S","Phase T","Phase AVG","Power Factor","Frequency"]
        // let unit =["KV","KV","KV","KV","KV","KV","KV","KV","A","A","A","A"," ","Hz"]
        
        // if(dt[0]["power_type"]=="2"){
        //     var_power=["VRN","VSN","VTN","VAVGN","VRS","VST","VRT","VAVG","IR","IS","IT","IAVG","KW","KVAR","PFAVG","F"]
        //     var_name=["Volt R-N","Volt S-N","Volt T-N","Volt R-S-T-N","Volt R-S","Volt S-T","Volt R-T","Volt R-S-T","Phase R","Phase S","Phase T","Phase AVG","Active Power","Reactive Power","Power Factor","Frequency"]
        //     unit =["KV","KV","KV","KV","KV","KV","KV","KV","A","A","A","A","MW","MVAR"," ","Hz"]
        // }// let json_data={}
        let machine_data={}
        let machine_params=[]
        machine_data["relay_type"]=dt[0]["nama"]
        machine_data["id_device"]=dt[0]["id_modbus"]
        machine_data["type"]=null
        machine_data["port_type"]=null
        machine_data["port_number"]=null
        machine_data["port_address"]=null
        machine_data["rack_location"]=dt[0]["bay_name"]
        machine_data["baudrate"]=null
        machine_data["ip_address"]=dt[0]["iplocal"]
        machine_data["bay_code"]=dt[0]["bay_code"]
        machine_data["dns"]=dt[0]["dns"]
        machine_data["netmask"]=dt[0]["netmask"]
        machine_data["gateway"]=dt[0]["gateway"]
        machine_data["stop_bits"]=null
        machine_data["parity"]=null
        machine_data["byte_size"]=null
        machine_data["kondisi"]=null
        machine_data["mode"]=null
        machine_data["gi_name"]=dt[0]["nama_gi"]
        jml_channel=  parseInt(dt[0]["total_port"])
       
        //machine_data["protocol"]=null
        //machine_data["iec_file"]=null
        //machine_data["scl_name"]=null
        //machine_data["scl_flag"]=null
        //machine_data["disturbance_type"]=null
        machine_data["machine_code"]=dt[0]["kode_mesin"]
        machine_data["ipserver"]=dt[0]["ipserver"]
        console.log(jml_channel)
       $bay_code= dt[0]["bay_code"];
        for (let i =0 ;i<jml_channel;i++){
            let json_data={}
            json_data["domain_id"]=`${dt[i]["kode_mesin"]}-0${dt[i]["port"]}`
            json_data["item_id"]=`CHN${dt[i]["port"]}`
            json_data["group"]=null
            json_data["active"]=1
            json_data["id_device"]=dt[i]["id_modbus"]
            json_data["ip_address"]=null
            json_data["alias"]=`${dt[i]["kode_mesin"]}${dt[0]["id_modbus"]}CHN${dt[i]["port"]}`
            json_data["name"]=`${dt[i]["nama_alat"]}`
            json_data["machine_code"]=`${dt[i]["kode_mesin"]}`
            json_data["register"]=0
            json_data["val"]=0
            json_data["type"]="DAS"
            json_data["port_type"]=0
            json_data["data_type"]="boolean"
            json_data["position"]=0
            json_data["poss_all"]=0
            json_data["max_value"]=0
            json_data["min_value"]=0
            json_data["treshold"]=0
            json_data["unit"]=0
            json_data["goal"]=0
            json_data["multiplier"]=0
            json_data["divider"]=1
            json_data["status_low"]=dt[i]["low"]
            json_data["status_high"]=dt[i]["high"]
            json_data["bay_code"]=dt[i]["bay_code"]
            machine_params.push(json_data)
           }
        for  (let i =0 ;i<var_power.length;i++){
            let json_data={}
            json_data["domain_id"]=`${dt[0]["kode_mesin"]}-${dt[0]["id_modbus"]}-${var_power[i]}`
            json_data["item_id"]=`${var_power[i]}`
            json_data["group"]=null
            json_data["active"]=1
            json_data["id_device"]=dt[0]["id_modbus"]
            json_data["ip_address"]=null
            json_data["alias"]=`${dt[0]["kode_mesin"]}${dt[0]["id_modbus"]}${var_power[i]}`
            json_data["name"]=`${var_name[i]}`
            json_data["machine_code"]=`${dt[0]["kode_mesin"]}`
            json_data["register"]=0
            json_data["val"]=0
            json_data["type"]="DAS"
            json_data["port_type"]=0
            json_data["data_type"]="float"
            json_data["position"]=0
            json_data["poss_all"]=0
            json_data["max_value"]=0
            json_data["min_value"]=0
            json_data["treshold"]=0
            json_data["unit"]=unit[i]
            json_data["goal"]=0
            json_data["multiplier"]=0
            json_data["divider"]=parseInt(var_divider[i])
            json_data["status_low"]=0
            json_data["status_high"]=0
            json_data["bay_code"]=dt[i]["bay_code"]
            machine_params.push(json_data)
        }
        urls=`http://${dt[0]["ipgipat"]}/configure/DAS`
        
        
        console.log(machine_params);
        console.log(machine_data)
       //      data: {"machine_params":machine_params,"machine_data":machine_data},
        $.ajax({
            type: "POST",
            url: urls,
             crossDomain: true,
          
            dataType: "json",
            //contentType : 'application/json',
          data: {"machine_params":machine_params,"machine_data":machine_data},
            success: function(msg){
                Swal.fire({
                position: 'top-center',
                icon: 'success',
                title: msg.msg,
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
                $("#valmodal").modal('hide')
            })
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("some error");
                alert(JSON.stringify(XMLHttpRequest))
                alert(JSON.stringify(errorThrown))
            }
            });

        });
    }                       
    $.ajax({
        method: "get",
        url: 'backend/webapi/viewsetting.php'
    }).done(function(msg) {
        let data = JSON.parse(msg)
        $('#code').val(data[0].kode_mesin)
        $('#bot_name').val(data[0].botname)
        $('#location').val(data[0].nama_gi)
        $('#name').val(data[0].nama)
        $('#bay_code').val(data[0].bay_code)
        $('#bay_name').val(data[0].bay_name)
        
        $('#systemdelay').val(data[0].system_delay)
       
        $('#ipserver').val(data[0].ipserver)
        $('#ipgipat').val(data[0].ipgipat)
        $('#ipaddress').val(data[0].iplocal)
        $('#netmask').val(data[0].netmask)
        $('#gateway').val(data[0].gateway)
        $('#dns').val(data[0].dns)
        if (data[0].dhcp == 1) {
            document.getElementById("dhcp").checked = true;
            //  document.getElementById("ipserver").disabled = true;
            // $('ipserver').attr('disabled', true);
        }
    })

    $.ajax({
        method: "get",
        url: 'api/display/<?php echo $user; ?>'
    }).done(function(msg) {
        let data = (msg)
        $('#port').val(data[0].total_port)
        $('#zoomH').val(data[0].zoom_h)
        $('#zoomW').val(data[0].zoom_w)
        $('#fontSize').val(data[0].font_size)
    })

    $.ajax({
        method: "get",
        url: 'api/meter/1'
    }).done(function(msg) {
        let data = (msg)

        $('#baudrate').val(data[0].baud_serial)
        $('#parity').val(data[0].parity_serial)
        $('#metertype').val(data[0].type)
        $('#idmeter').val(data[0].id_modbus)
        $('#mqttserver').val(data[0].server_mqtt)
        $('#mqttuser').val(data[0].user_mqtt)
        $('#mqttpass').val(data[0].pass_mqtt)
        $('#mqttport').val(parseInt(data[0].port_mqtt))

    })

    $.ajax({
        method: "get",
        url: 'api/users/<?php echo $user; ?>'
    }).done(function(msg) {
        let data = (msg)
        $('#username').val(data[0].username)
        $('#passworduser').val(data[0].password)
    })


    $.ajax({
        method: "get",
        url: 'api/wifi'
    }).done(function(msg) {
        let data = (msg)
        $('#ssid').val(data[0].ssid)
        $('#password').val(data[0].pass)
    })

    $.ajax({
        method: "get",
        url: 'api/tele'
    }).done(function(msg) {
        let data = (msg)
        $('#chatid').val(data[0].chatid)
        $('#api').val(data[0].api)
    })

    //keyboard service
    $('#ipserver').click(function() {

        $('#key').show()
        $('#key').removeClass('pos2')
        $('#key').removeClass('pos3')
        $('#key').removeClass('pos4')
        $('#key').removeClass('pos5')
        $('#key').addClass('pos1')
    })
    $('#ipaddress').click(function() {
        $('#key').show()
        $('#key').removeClass('pos1')
        $('#key').removeClass('pos3')
        $('#key').removeClass('pos4')
        $('#key').removeClass('pos5')
        $('#key').addClass('pos2')
    })
    $('#netmask').click(function() {
        $('#key').show()
        $('#key').removeClass('pos1')
        $('#key').removeClass('pos2')
        $('#key').removeClass('pos4')
        $('#key').removeClass('pos5')
        $('#key').addClass('pos3')
    })
    $('#gateway').click(function() {
        $('#key').show()
        $('#key').removeClass('pos1')
        $('#key').removeClass('pos2')
        $('#key').removeClass('pos3')
        $('#key').removeClass('pos5')
        $('#key').addClass('pos4')
    })
    $('#dns').click(function() {
        $('#key').show()
        $('#key').removeClass('pos1')
        $('#key').removeClass('pos2')
        $('#key').removeClass('pos3')
        $('#key').removeClass('pos4')
        $('#key').addClass('pos5')
    })

    var keyboard = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['`', '`', 192, 0, true],
                    ['1', '1', 49, 0, false],
                    ['2', '2', 50, 0, false],
                    ['3', '3', 51, 0, false],
                    ['4', '4', 52, 0, false],
                    ['5', '5', 53, 0, false],
                    ['6', '6', 54, 0, false],
                    ['7', '7', 55, 0, false],
                    ['8', '8', 56, 0, false],
                    ['9', '9', 57, 0, false],
                    ['0', '0', 48, 0, false],
                    ['-', '-', 189, 0, false],
                    ['=', '=', 187, 0, false],
                    ['q', 'q', 81, 0, true],
                    ['w', 'w', 87, 0, false],
                    ['e', 'e', 69, 0, false],
                    ['r', 'r', 82, 0, false],
                    ['t', 't', 84, 0, false],
                    ['y', 'y', 89, 0, false],
                    ['u', 'u', 85, 0, false],
                    ['i', 'i', 73, 0, false],
                    ['o', 'o', 79, 0, false],
                    ['p', 'p', 80, 0, false],
                    ['[', '[', 219, 0, false],
                    [']', ']', 221, 0, false],
                    ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true],
                    ['s', 's', 83, 0, false],
                    ['d', 'd', 68, 0, false],
                    ['f', 'f', 70, 0, false],
                    ['g', 'g', 71, 0, false],
                    ['h', 'h', 72, 0, false],
                    ['j', 'j', 74, 0, false],
                    ['k', 'k', 75, 0, false],
                    ['l', 'l', 76, 0, false],
                    [';', ';', 186, 0, false],
                    ['&#39;', '\'', 222, 0, false],
                    ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true],
                    ['z', 'z', 90, 0, false],
                    ['x', 'x', 88, 0, false],
                    ['c', 'c', 67, 0, false],
                    ['v', 'v', 86, 0, false],
                    ['b', 'b', 66, 0, false],
                    ['n', 'n', 78, 0, false],
                    ['m', 'm', 77, 0, false],
                    [',', ',', 188, 0, false],
                    ['.', '.', 190, 0, false],
                    ['/', '/', 191, 0, false],
                    ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true],
                    ['Space', '32', 32, 12, false],
                    ['Clear', '46', 46, 3, false],
                    ['Cancel', '27', 27, 3, false]
                ]
            ]
        ]
    }
    $('input.jQKeyboard').initKeypad({
        'keyboardLayout': keyboard
    });

    $('#btn-del').click(function() {
        $("#valmodal").modal('show')
        $("#text-config").text("Do you want to delete data?")
        $("#conf-text").text("del")
    });
    $('#btn-restart').click(function() {
        $("#valmodal").modal('show')
        $("#text-config").text("Do you want to Restart device?")
        $("#conf-text").text("rest")
    });
    $('#btn-yes').click(function() {
        let conf = $("#conf-text").text()
        if (conf == "del") {
            deletedevice();
        } else if (conf == "rest") {
            restartdevice()
        }
    });
    $('#btn-no').click(function() {
        $("#valmodal").modal('hide')
    })

    function deletedevice() {
        $.ajax({
            method: "get",
            url: 'backend/webapi/deletelog.php'
        }).done(function() {
            Swal.fire({
                position: 'top-center',
                icon: 'success',
                title: 'Your data has been deleted',
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
                $("#valmodal").modal('hide')
            })

        })
    }

    function restartdevice() {
        $.ajax({
            method: "get",
            url: 'backend/webapi/reboot.php'
        }).done(function() {
            Swal.fire({
                position: 'top-center',
                icon: 'success',
                title: 'device is restarted',
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
                $("#valmodal").modal('hide')
            })

        })
    }
    $('#btncancel').click(function() {
        gotohome()
    });

    $('#btnsaveajah').click(function() {
        let port = $('#port').val();
        let zoomW = $('#zoomW').val();
        let zoomH = $('#zoomH').val();
        let fontSize = $('#fontSize').val();
        $.ajax({
            method: "post",
            // headers: {'Content-Type': 'application/json'}, 
            // body: JSON.stringify(data),
            url: 'api/params/<?php echo $user; ?>',
            data: {
                total_port: port,
                zoom_w: zoomW,
                zoom_h: zoomH,
                font_size: fontSize,

            }
        }).done(function(msg) {

            let data = (msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Your data has been saved',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });


    $('#btnsaveaccount').click(function() {
        let username = $('#username').val();
        let password = $('#passworduser').val();
        $.ajax({
            method: "post",
            // headers: {'Content-Type': 'application/json'}, 
            // body: JSON.stringify(data),
            url: 'api/users/<?php echo $user; ?>',
            data: {
                username: username,
                password: password,
            }
        }).done(function(msg) {

            let data = (msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Your account has been change',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });
    $('#btnsavetele').click(function() {
        let chatid = $('#chatid').val();
        let api = $('#api').val();
        let flag = 1;
        $.ajax({
            method: "post",
            // headers: {'Content-Type': 'application/json'}, 
            // body: JSON.stringify(data),
            url: 'api/tele/1',
            data: {
                chatid: chatid,
                api: api,

            }
        }).done(function(msg) {

            let data = (msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Telegram setting has been change',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });
    $('#btngetchatid').click(function() {
        let api = $('#api').val()
        
        link='https://api.telegram.org/bot'+api+'/getUpdates'
        $.ajax({
            method: "get",
            // headers: {'Content-Type': 'application/json'}, 
            // body: JSON.stringify(data),
            url: link,
            
        }).done(function(msg) {
           
            if(msg.ok){
                if(msg.result.length<1){
                    alert("Data Not Found")
               }
               else{
                    jml = msg.result.length-1
                    console.log(jml)
                    console.log(msg.result[jml].message.chat)
                    idChat=msg.result[jml].message.chat.id
                    $('#chatid').val(idChat)
                    alert("Chat id Found : "+idChat)
               }
            }else{
                alert("api key Invalid")
            }
        }).fail(()=>{
            alert("Connection Error")
        })
    });
    $('#btnsavewifi').click(function() {
        let ssid = $('#ssid').val();
        let password = $('#password').val();
        let flag = 1;
        $.ajax({
            method: "post",
            url: 'api/wifi/1',
            data: {
                ssid: ssid,
                pass: password,
                flag: flag,

            }
        }).done(function(msg) {

            let data = (msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Wifi setting has been change',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });

    $('#btnsavedelay').click(function() {
        let systemdelay = $('#systemdelay').val();
        $.ajax({
            method: "post",
            // headers: {'Content-Type': 'application/json'}, 
            // body: JSON.stringify(data),
            url: 'api/delay/1',
            data: {
                system_delay: systemdelay,
            }
        }).done(function(msg) {

            let data = (msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'System delay has been change',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });

    $('#btnsavemeter').click(function() {
        let baudrate = parseInt($('#baudrate').val());
        let metertype= parseInt($('#metertype').val());
        let idmeter= parseInt($('#idmeter').val());  
        let mqttserver=$('#mqttserver').val()
        let mqttuser=$('#mqttuser').val()
        let mqttpass=$('#mqttpass').val()
        let mqttport=$('#mqttport').val()
        let parity =$('#parity').val()
        $.ajax({
            method: "post",
            url: 'api/meter/1',
            data: {
                baud_serial: baudrate,
                type:metertype,
                id_modbus : idmeter,
                server_mqtt:mqttserver,
                user_mqtt:mqttuser,
                pass_mqtt:mqttpass,
                port_mqtt:mqttport,
                parity_serial:parity,
                flag:1


            }
        }).done(function(msg) {
            let data = (msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'System  has been change',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    })


    $('#btnsavelan').click(function() {
        let ipserver = $('#ipserver').val();
        let ipaddress = $('#ipaddress').val();
       
        let netmask = $('#netmask').val();
        let gateway = $('#gateway').val();
        let dns = $('#dns').val();
        let loc = $('#location').val();
        let name = $('#name').val();
        let ipgipat =$('#ipgipat').val()
        let dhcp = $('#dhcp').is(':checked') ? $('#dhcp').val() : 0;
        let bay_code =$('#bay_code').val()
        let bay_name = $('#bay_name').val()
        $.ajax({
            method: "post",
            url: 'backend/webapi/updatesetting.php',
            data: {
                ipserver: ipserver,
                ipaddr: ipaddress,
                netmask: netmask,
                gateway: gateway,
                dns: dns,
                loc: loc,
                name: name,
                dhcp: dhcp,
                ipgipat:ipgipat,
                bay_code:bay_code,
                bay_name:bay_name,
                

            }
        }).done(function(msg) {

            let data = JSON.parse(msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Your data has been saved',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });



    $('#btnsave').click(function() {
        let ipserver = $('#ipserver').val();
        let ipaddress = $('#ipaddress').val();
        let netmask = $('#netmask').val();
        let gateway = $('#gateway').val();
        let dns = $('#dns').val();
        let loc = $('#location').val();
        let name = $('#name').val();
        let bay_code = $('#bay_code').val();
        let bay_name = $('#bay_name').val();
        let systemdelay = $('#systemdelay').val();
        let dhcp = $('#dhcp').is(':checked') ? $('#dhcp').val() : 0;
        let ipgipat=$('#ipgipat').val();
        $.ajax({
            method: "post",
            url: 'backend/webapi/updatesetting.php',
            data: {
                ipserver: ipserver,
                ipaddr: ipaddress,
                netmask: netmask,
                gateway: gateway,
                dns: dns,
                bay_name:bay_name,
                bay_code:bay_code,
                loc: loc,
                name: name,
                systemdelay: systemdelay,
                dhcp: dhcp,
                ipgipat:ipgipat

            }
        }).done(function(msg) {

            let data = JSON.parse(msg)
            if (data.code == 1) {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Your data has been saved',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    gotohome()
                })
            } else {
                alert(msg)
            }
            // alert('update success')
        })
    });


    $('#btnadduser').click(function() {
        let nama_lengkap = $('#namaLengkap').val();
        let username = $('#addUsername').val();
        let password = $('#addPassword').val();
        let level = $('#addlevel option:selected').val()
        if (nama_lengkap != '' || username != '' || password != '') {
            $.ajax({
                method: "post",
                url: 'api/users',
                data: {
                    nama_lengkap: nama_lengkap,
                    username: username,
                    password: password,
                    level: level,
                    email: 'example@pln.co.id',
                    blokir: 'N',

                }
            }).done(function(msg) {

                let data = (msg)
                if (data.code == 1) {
                    Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Your data has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function() {
                        // window.location.href("/das/beranda");
                        gotohome();
                        // gotohome();
                    })
                } else {
                    alert(msg)
                }
                // alert('update success')
            })
        }
    });
    function getParam(){
        $.ajax({
            method: "get",
            url: 'api/Power_param',
           
        }).done(function(msg) {
            $('#param_body').empty()
            $('#param_length').text(msg.length)
            for(let i =0 ; i<msg.length;i++){
                console.log(msg[i].param_id)
                $('#param_body').append(` <div class="col-lg-12">
                                    <div>
                                        <label class="jQKeyboard col-md-3" for="api">Parameter Id</label>
                                        <input class="col-lg-6 jQKeyboard" type="text" name="id_param${i}" id="id_param${i}" value="${msg[i].param_id}" disabled >
                                    </div>
                                        <label class="jQKeyboard col-md-3" for="api">Parameter Name</label>
                                        <input class="col-lg-6 jQKeyboard" type="text" name="name_param${i}" id="name_param${i}" value="${msg[i].param_name}">
                                    <div>
                                        <label class="jQKeyboard col-md-3" for="api">Unit</label>
                                        <input class="col-lg-6 jQKeyboard" type="text" name="unit_param${i}" id="unit_param${i}" value="${msg[i].param_unit}">
                                    </div>
                                     <div>
                                        <label class="jQKeyboard col-md-3" for="divider">Divider</label>
                                        <input class="col-lg-6 jQKeyboard" type="number" name="divider${i}" id="divider${i}" value="${msg[i].divider}">
                                    </div>
                                    <hr>
                                </div> <br  >`)
            }
          
        })
    }
    $('#btnsaveparam').click(function() {
        let param_length= parseInt($('#param_length').text())
        let param_id= []
        let param_name=[]
        let param_unit =[]
        let divider =[]
        let dataJson=[]
        for (let i =0;i<param_length ;i++){
            // param_id.push($(`#id_param${i}`).val())
            // param_name.push($(`#name_param${i}`).val())
            // param_unit.push($(`#unit_param${i}`).val())
            let dt ={}
            dt.param_id=$(`#id_param${i}`).val()
            dt.param_name=$(`#name_param${i}`).val()
            dt.param_unit =$(`#unit_param${i}`).val()
            dt.divider =$(`#divider${i}`).val()
            dataJson.push(dt)
         
        }
        console.log(dataJson)
        $.ajax({
            method: "post",
            url: 'api/Power_param',
            data:{data:JSON.stringify(dataJson)}
           
            //data:{data:dataJson}
           
        }).done(function(msg) {
            
            console.log(msg)
            Swal.fire({
                position: 'top-center',
                icon: 'success',
                title:msg,
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
                
            })
       
        }) .fail(function(jqXHR, textStatus, errorThrown) {
            // This function is executed if the request fails
            console.error("AJAX request failed: " + textStatus, errorThrown);

            
        });
       
     

    })
    $(document).ready(function() {

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };


        var table2 = $("#tableUser").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#tableUser_filter input')
                    .off('.DT')
                    .on('input.DT', function() {
                        api.search(this.value).draw();
                    });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            lengthMenu: [5],
            searching: false,
            processing: true,
            serverSide: true,
            ajax: {
                "url": "users/json",
                "type": "POST"
            },

            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                $('td:eq(0)', row).html();
            }

        });






    });
</script>