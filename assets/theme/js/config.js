// host = '172.16.153.122';	// hostname or IP address
var buffData = "";
var param = [];
gg = [];
gaug = {};
//const formatter = new Intl.NumberFormat("en-US");
// var gg = [
// 	"F",
// 	"VRN",
// 	"VSN",
// 	"VTN",
// 	"VAVG",
// 	"VRS",
// 	"VST",
// 	"VRT",
// 	"IR",
// 	"IS",
// 	"IT",
// 	"IN",
// 	"PFR",
// 	"PFS",
// 	"PFT",
// ];
//var satuan = ["Hz","V","V","V","V","V","V","V","A","A","A","A","","",""]
var max = [
	400, 400, 400, 400, 400, 400, 400, 400, 1000, 1000, 1000, 1000, 1000, 1000,
	1000, 1, 100, 1,
];
startConnect();
function startConnect() {
	// Generate a random client ID
	clientID = "clientID-" + parseInt(Math.random() * 100000);
	// Fetch the hostname/IP address and port number from the form
	host = location.hostname;
	//console.log(url)
	//host = url[3]
	//host = "203.194.112.238";

	port = "8083";
	client = new Paho.MQTT.Client(host, Number(port), clientID);
	// Set callback handlers
	client.onConnectionLost = onConnectionLost;
	client.onMessageArrived = onMessageArrived;
	var options = {
		useSSL: false,
		cleanSession: true,
		timeout: 3,
		onSuccess: onConnect,
		userName: "das",
		password: "das12345",
	};
	client.connect(options);
}
// Called when the client connects
function onConnect() {
	// Fetch the MQTT topic from the form
	topic = "DASPOWER/#";
	client.subscribe(topic);
}

// Called when the client loses its connection
function onConnectionLost(responseObject) {
	//document.getElementById("messages").innerHTML = 'ERROR: Connection lost';
	console.log(responseObject.errorMessage);
	if (responseObject.errorCode !== 0) {
		console.log(responseObject.errorMessage);
	}
	startConnect();
}

function onMessageArrived(message) {
	// /console.log(message.payloadString);
	//console.log(message.destinationName);
	let dest = message.destinationName.split("/");
	if (dest.length > 4) {
		let dt = JSON.parse(message.payloadString);
		if (dt.dataType == "float") {
			//console.log(dt);
			try {
				let alias = dt.alias;
				let divider = $(`.divider-${alias}`).text();
				if (divider != NaN || divider != undefined) {
					val = (parseFloat(dt.val) / parseFloat(divider)).toFixed(3);
					formated_val = val.toString().replace(".", ",");
					$(`.${alias}`).text(formated_val);
					gaug[`${alias}`].refresh(val);
				}
				console.log(divider);
			} catch (error) {
				console.log(error);
			}
		}
	}

	// buffData = JSON.parse(message.payloadString);
	// console.log(param);
	// for (let i = 0; i < param.length; i++) {
	// 	try {
	// 		//gg[i].refresh(buffData[param[i]]);
	// 		let divider = parseFloat($(`.divider-${param[i]}`).text());
	// 		let val = (parseFloat(buffData[param[i]]) / divider).toFixed(3);
	// 		//console.log(`divider-${param[i]}`);
	// 		if (val != NaN) {
	// 			gg[i].refresh(buffData[param[i]]);
	// 		}
	// 		//console.log(val);
	// 		$("." + param[i]).text(val);
	// 	} catch (error) {
	// 		//param[i];
	// 	}
	//}
	// } catch (err) {
	// 	console.log(err);
	// }
}
function sendMessage(ia, tp) {
	mia = new Paho.MQTT.Message(ia);
	mia.destinationName = tp;
	client.send(mia);
}
// Called when the disconnection button is pressed
function startDisconnect() {
	client.disconnect();
	//document.getElementById("messages").innerHTML += '<span>Disconnected</span><br/>';
}
